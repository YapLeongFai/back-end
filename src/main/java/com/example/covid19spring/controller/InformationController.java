package com.example.covid19spring.controller;

import com.example.covid19spring.service.InformationService;
import com.example.covid19spring.model.Information;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/covid19")
public class InformationController {

    @Autowired
    private InformationService informationService;

    @RequestMapping("/information")
    public List<Information> all_information(){
        return informationService.getAllInfo();
    }

    @RequestMapping("/information/{keyword}")
    public Information getInformation(@PathVariable("keyword") String keyword){
        return informationService.getInformation(keyword);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/information")
    public void addInformation(@RequestBody Information information){
        informationService.addInformation(information);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/information/{keyword}")
    public void updateInformation(@RequestBody Information information, @PathVariable("keyword") String keyword){
        informationService.updateInformation(information, keyword);
    }
    
    @RequestMapping(method = RequestMethod.DELETE, value = "/information/{keyword}")
    public void deleteInformation(@PathVariable("keyword") String keyword){
        informationService.deleteInformation(keyword);
    }
}
