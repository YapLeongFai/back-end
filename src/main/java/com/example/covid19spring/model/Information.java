package com.example.covid19spring.model;

public class Information {
    private String keyword;
    private String title;
    private String subTitle;
    private String contentRow1;
    private String contentRow2;
    private String contentRow3;
    private String contentRow4;
    private String contentRow5;
    private String contentRow6;

    public Information(){

    }

    public Information(String keyword, String title, String subTitle, String contentRow1, String contentRow2,String contentRow3,String contentRow4, String contentRow5, String contentRow6){
        this.keyword=keyword;
        this.title=title;
        this.subTitle=subTitle;
        this.contentRow1=contentRow1;
        this.contentRow2=contentRow2;
        this.contentRow3=contentRow3;
        this.contentRow4=contentRow4;
        this.contentRow5=contentRow5;
        this.contentRow6=contentRow6;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getContentRow1() {
        return contentRow1;
    }

    public void setContentRow1(String contentRow1) {
        this.contentRow1 = contentRow1;
    }

    public String getContentRow2() {
        return contentRow2;
    }

    public void setContentRow2(String contentRow2) {
        this.contentRow2 = contentRow2;
    }

    public String getContentRow3() {
        return contentRow3;
    }

    public void setContentRow3(String contentRow3) {
        this.contentRow3 = contentRow3;
    }

    public String getContentRow4() {
        return contentRow4;
    }

    public void setContentRow4(String contentRow4) {
        this.contentRow4 = contentRow4;
    }

    public String getContentRow5() {
        return contentRow5;
    }

    public void setContentRow5(String contentRow5) {
        this.contentRow5 = contentRow5;
    }

    public String getContentRow6() {
        return contentRow6;
    }

    public void setContentRow6(String contentRow6) {
        this.contentRow6 = contentRow6;
    }
}
