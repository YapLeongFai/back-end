package com.example.covid19spring.service;

import com.example.covid19spring.model.Information;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class InformationService {

    private List<Information> informationList = new ArrayList<>(Arrays.asList(
            new Information("symptoms",
                    "What are the symptoms of COVID-19?",
                    "",
                    "- Fever",
                    "- Tiredness",
                    "- Dry cough",
                    "*Some people become infected but don't develop any symptoms and don't feel unwell",
                    "*Around 1 out of every 6 people who gets COVID-19 becomes seriously ill and develop s difficult breathing",
                    "Older people, and those with underlying medical problems like high blood pressure, heart problems or diabetes, " +
                            "are more likely to develop serious illness"),
            new Information("spread",
                    "How does COVID-19 spread?",
                    "",
                    "People can catch COVID-19 from others who have the virus. The disease can spread from person to person through" +
                            "small droplets from the nose or mouth which are spread when a person with COVID-19 coughs or exhales. These droplets" +
                            "land on objects and surfaces around the person. Other people then catch COVID-19 by touching these objects or surfaces," +
                            "then touching their eyes, nose or mouth. People can also catch COVID-19 if they breathe in droplets from a person" +
                            "with COVID-19 who coughs out or exhales droplets. This is why it is important to stay more than 1 meter (3 feet)" +
                            "away from a person who is sick.",
                    "",
                    "",
                    "",
                    "",
                    ""),
            new Information("prevent",
                    "How to protect yourself and prevent the spread of disease?",
                    "You can reduce your chances of being infected or spreading COVID-19 by taking some simple precautions:",
                    "- Regularly and thoroughly clean your hands with an alcohol-based hand rub or wash them with soap and water.",
                    "- Maintain at least 1 metre (3 feet) distance between yourself and anyone who is coughing or sneezing.",
                    "- Avoid touching eyes, nose and mouth.",
                    "- Make sure you, and the people around you, follow good respiratory hygiene. This means covering your mouth and nose" +
                            "with your bent elbow or tissue when you cough or sneeze. Then dispose of the used tissue immediately.",
                    "- Stay home if you feel unwell. If you have a fever, cough and difficulty breathing, seek medical attention and call" +
                            "in advance. Follow the directions of your local health authority.",
                    "- Keep up to date on the latest COVID-19 hotspots (cities or local areas where COVID-19 is spreading widely). If" +
                            "possible, avoid traveling to places – especially if you are an older person or have diabetes, heart or lung disease.")
    ));

    public Information getInformation(String keyword){
        return informationList.stream().filter(information -> information.getKeyword().equals(keyword)).findFirst().get();
    }

    public void addInformation(Information information){
        informationList.add(information);
    }

    public void updateInformation(Information information, String keyword){
        int count = 0;
        for(Information info:informationList){
            if(info.getKeyword().equals(keyword)){
                informationList.set(count, information);
            }
            count++;
        }
    }

    public void deleteInformation(String keyword){
        informationList.removeIf(information -> information.getKeyword().equals(keyword));
    }

    public List<Information> getAllInfo(){
        return informationList;
    }
}
