package com.example.covid19spring.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.example.covid19spring.model.JwtRequest;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {
	private List<JwtRequest> userDetailList = new ArrayList<>(Arrays.asList(
			new JwtRequest("abcd1234", "$2a$10$PCdnrF182muRmqNuSJ5fUe8nkXglX5eDonzF4BS81KypqoNEIdF6y"),
			new JwtRequest("1234abcd", "$2a$10$zJNXL4XcW3MLcA0gxwZqCuVCGwRfrZkF481nsncDH4xmYP3mwV7TG")
	));

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		String user = null;
		String pass = null;
		for (JwtRequest jwtRequest : userDetailList) {
			if (jwtRequest.getUsername().equals(username)) {
				user = jwtRequest.getUsername();
				pass = jwtRequest.getPassword();
				break;
			}
		}
		if (user == null && pass == null) {
			throw new UsernameNotFoundException("User not found with username: " + username);
		}

		return new User(user, pass, new ArrayList<>());
	}

	public List<JwtRequest> getAllUserDetail() {
		return userDetailList;
	}
}